using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Juego : MonoBehaviour
{
    public Text UI;
    public List<Sprite> imagenes;
    public Image imagenUI;
    void Update()
    {
        UI.text = GameManager.monedas.ToString();
        CambiarImagen();
    }
    public void Aumentar()
    {
        GameManager.monedas += GameManager.multiplicar;
    }

    public void Tienda(int num)
    {
        if(num==1 && GameManager.monedas >= 10) 
        {
            GameManager.multiplicar += 1;
            GameManager.monedas -= 10;
        }
       
        if (num == 2 && GameManager.monedas >= 125)
        {
            GameManager.multiplicar += 10;
            GameManager.monedas -= 125;
        }
        
        if (num == 3 && GameManager.monedas >= 1000)
        {
            GameManager.multiplicar += 100;
            GameManager.monedas -= 1000;
        }



    }
    public void CambiarImagen()
    {
        UI.text = GameManager.monedas.ToString();

        if (GameManager.monedas >= 500 && GameManager.monedas < 1000)
        {
            imagenUI.sprite = imagenes[0]; // Cambia a la primera imagen de la lista
        }
        else if (GameManager.monedas >= 1000 && GameManager.monedas < 3000)
        {
            imagenUI.sprite = imagenes[1]; // Cambia a la segunda imagen de la lista
        }
        else if (GameManager.monedas >= 3000 && GameManager.monedas < 10000)
        {
            imagenUI.sprite = imagenes[2]; // Cambia a la segunda imagen de la lista
        }
        else if (GameManager.monedas >= 10000)
        {
            imagenUI.sprite = imagenes[3]; // Cambia a la segunda imagen de la lista
        }
    }
}
